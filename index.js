var logger = require('utils/logger');
var child_p = require('child_process');

var server = null;

function server_spawn(serverjs, configfile) {
    logger.info('server process js: %s', serverjs);
    logger.info('config file: %s', configfile);

    server = child_p.spawn('node', [ serverjs, configfile ]);

    server.on('exit', function(code){
        if (code !== 0) {
            logger.warn('server exited with code: %d, respawn', code);

            server_spawn(serverjs, configfile);
        }
    });
}

function main(argv) {
    // trace command line args
    logger.trace('chamberlain argv[0]: ', argv[0]);
    logger.trace('chamberlain argv[1]: ', argv[1]);
    logger.trace('chamberlain argv[2]: ', argv[2]);
    logger.trace('chamberlain argv[3]: ', argv[3]);

    // log levels
    logger.trace('trace');
    logger.debug('debug');
    logger.info('info');
    logger.warn('warn');
    logger.error('error');
    logger.fatal('fatal');

    logger.info('Chamberlain started');

    // handle uncaught exceptions
    process.on('uncaughtException', function(err) {
        logger.error('Error: %s', err.message);
    });

    server_spawn(__dirname + '/modules/server.js', argv[2]);

    // server.start(port);
 
    /* demostrate how to use a domain, not a proper place tho
    var d = domain.create();

    d.on('error', function(err) {
        logger.error('Error: %s', err.message);
    });

    d.run(function() {
        server.start(port);
    });
    */

    process.on('SIGTERM', function() {
        logger.info('Caught termination signal, chamberlain is going to shutdown');

        server.kill();
        process.exit(0);
    });
}

main(process.argv.slice(0));
