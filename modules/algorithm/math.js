// use loop instead to boost performance by reduce the function calls
function factorial(n) {
    if (n === 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

exports.factorial = factorial;
