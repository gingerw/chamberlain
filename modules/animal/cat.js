var express = require('express');

exports.router = function(routerin) {
    var router = null;

    if (routerin != null) {
        router = routerin;
    } else {
        router = express.Router();

        router.get('/', function(req, res, next) {
            res.contentType('json');
            res.send(JSON.stringify({animal: "cat"}));
        });
    }

    return router;
}
