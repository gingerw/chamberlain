var express = require('express');

exports.router = function() {
    router = express.Router();
    router.get('/bark', function(req, res, next) {
        res.contentType('json');
        res.send(JSON.stringify({animal: "dog"}));
    });

    return router;
}
