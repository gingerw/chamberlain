var fs = require('fs');
var path = require('path');
var iconv = require('iconv-lite');

/*
 * file stats: fs.stat, fs.chmod, fs.chown etc.
 * file io: fs.readFile, fs.readdir, fs.mkdir etc.
 * system calls: fs.open, fs.read, fs.write, fs.close etc.
 */

// copy small file
exports.cp = function(src, dst) {
    fs.writeFileSync(dst, fs.readFileSync(src));
}

// copy large file
exports.copy = function(src, dst) {
    fs.createReadStream(src).pipe(fs.createWriteStream(dst));
}

// comprehensive exmaple
exports.safe_copy = function(src, dst) {
    var rs = fs.createReadStream(src);
    var ws = fs.createWriteStream(dst);

    rs.on('data', function(chunk) {
        if (ws.write(chunk) === false) {
            rs.pause();
        }
    });

    rs.on('end', function() {
        ws.end();
    });

    ws.on('drain', function() {
        rs.resume();
    });
}

// all fs module api has its corresponding sync version
function async(pathname) {
    fs.readFile(pathname, function(err, data) {
        if (err) {
            // deal with error
        } else {
            // deal with data
        }
    });
}
function sync(pathname) {
    try {
        var data = fs.readFileSync(pathname);
        // deal with data
    } catch (err) {
        // deal with error
    }
}

// path operations
exports.path_eg = function() {
    // path.normalize
    var cache = {};

    function store(key, val) {
        cache[path.normalize(key)] = val;
    }

    store('foo/bar', 1);
    store('foo/baz/../bar', 2);
    console.log(cache); // => {"foo/bar": 2}

    // path.join
    console.log(path.join('foo/', 'baz/', '../bar')); // => "foo/bar"

    // path.extname
    console.log(path.extname('foo/bar.js')); // => ".js"
}

function traverse_sync(dir, callback) {
    fs.readdirSync(dir).forEach(function (file) {
        var pathname = path.join(dir, file);

        if (fs.statSync(pathname).isDirectory()) {
            traverse_sync(pathname, callback);
        } else {
            callback(pathname);
        }
    });
}
exports.traverse_sync = traverse_sync;

/* bad impl
function traverse(dir, callback, finish) {
    fs.readdir(dir, function(err, files) {
        (function next(i) {
            if ( i < files.lenth) {
                var pathname = path.join(dir, files[i]);

                fs.stat(pathname, function(err, stats) {
                    if (stats.isDirectory()) {
                        traverse(pathname, callback, function() {
                            next(i + 1);
                        });
                    } else {
                        callback(pathname, function() {
                            next(i + 1);
                        });
                    }
                });
            } else {
                finish && finish();
            }
        }(0));
    });
}
exports.traverse = traverse;
*/

exports.readTextWithoutBOM = function(pathname) {
    var bin = fs.readFileSync(pathname);

    if (bin[0] === 0xEF && bin[1] === 0xBB && bin[2] === 0xBF) {
        bin = bin.slice(3);
    }

    return bin.toString('utf-8');
}

exports.readTextGBK = function(pathname) {
    var bin = fs.readFileSync(pathname);

    return iconv.decode(bin, 'gbk');
}
