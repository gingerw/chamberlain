var logger = require('utils/logger');
var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var url = require('url');

var cat = require('animal/cat');
var dog = require('animal/dog');

function start(port) {
    logger.info('Server is starting');
    
    var app = express();

    // set the static files location /public/img will be /img for users
    //app.use(express.static(__dirname + '/../public'));

    // log every request to the console
    //app.use(morgan('dev')); 

    logger.debug('Overrideing "Express" logger');
    app.use(require('morgan')({ "stream": logger.stream }));
    logger.debug('Overrided"Express" logger');

    // parse application/x-www.form-urlencoded
    app.use(bodyParser.urlencoded({extended: false}));

    // parse application/json
    app.use(bodyParser.json());

    var webroot = express.Router();
    webroot.get('/', function(req, res, next) {
        // get url parameters
        var params = url.parse(req.url, true).query;
        var p_a = params.a;
        var p_b = params.b;

        res.contentType('json');
        res.send(JSON.stringify({status: "success", param_a: p_a, param_b: p_b}));
    });
    webroot.put('/', function(req, res, next) {
        var animal = req.body.animal;

        logger.debug("Animal is " + animal);

        res.contentType('json');
        res.send(JSON.stringify({status: "success"}));
    });

    app.use('/', webroot);
    app.use('/cat', cat.router(null));
    app.use('/dog', dog.router());

    app.listen(port, function() {
        logger.info("Listening on port " + port);
    });

    logger.info('Server started');
}

function shutdown(code) {
    logger.info("Server is shutting down");

    process.exit(code);
}

function main(argv) {
    // trace command line args
    logger.trace('server argv[0]: ', argv[0]);
    logger.trace('server argv[1]: ', argv[1]);
    logger.trace('server argv[2]: ', argv[2]);
    logger.trace('server argv[3]: ', argv[3]);

    // read configuration file
    logger.debug('loading config file');

    var config = JSON.parse(fs.readFileSync(argv[0], 'utf-8')),
        port = config.port || 80;

    logger.debug('config file loaded, port: %d', port);

    // handle system TERM signal
    process.on('SIGTERM', function() {
        logger.info('Caught termination signal, server is going to shutdown');

        shutdown(0);
    });

    // start the server
    start(port);
}

main(process.argv.slice(2));
