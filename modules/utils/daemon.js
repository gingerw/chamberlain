function spawn(module) {
    var worker = child_process.spawn('node', [ module ]);

    worker.on('exit', function(code) {
        if (code !== 0) {
            spawn(module);
        }
    });
}

exports.spawn = spawn;
