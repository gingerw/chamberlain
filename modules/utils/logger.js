var winston = require('winston');
var moment = require('moment');
/*
require('rolling-file-transport');

winston.loggers.add('rollingFile', {
    rollingFile: {
        filename: '/path/to/my/filename.log', // filename.<date>.log
        level: 'info',
        timestamp: true,
        maxFiles: 10,
        json: false
    }
});
*/

function time_format() {
    return moment().format('YYYY-MM-DD HH:mm:ss.SSS ZZ'); // '2014-07-03 20:14:28.500 +0900'
}

// set up logger

var logdir = __dirname + '/../../log';

var customColors = {
    trace: 'white',
    debug: 'blue',
    info: 'green',
    warn: 'yellow',
    error: 'red',
    fatal: 'red'
};

var logger = new winston.Logger({
    colors: customColors,
    levels: {
        trace: 0,
        debug: 1,
        info: 2,
        warn: 3,
        error: 4,
        fatal: 5
    },
    transports: [
        new winston.transports.Console ({
            name: 'cmd',
            level: 'debug',
            timestamp: time_format,
            datePattern: '.yyyy-MM-dd',
            colorize: true,
            label: 'main logger',
            handleExceptions: true,
            prettyPrint: true,
            silent: false
        }),
        new winston.transports.File({
            name: 'main',
            level: 'info',
            filename: logdir + '/chamberlain.log',
            json: false,
            timestamp: time_format,
            datePattern: '.yyyy-MM-dd',
            maxfiles: 10,
            maxsize: 1024 * 1012 * 10, // 10MB
            handleExceptions: false 
        }),
        new winston.transports.File({
            name: 'debug',
            level: 'trace',
            filename: logdir + '/debug.log',
            json: true,
            timestamp: time_format,
            datePattern: '.yyyy-MM-dd',
            maxfiles: 10,
            maxsize: 1024 * 1012 * 10, // 10MB
            handleExceptions: false 
        })
    ],
    exceptionHandlers: [
        new winston.transports.File({
            filename: logdir + '/exceptions.log',
            json: false,
            timestamp: true,
            maxfiles: 10,
            maxsize: 1024 * 1024 * 10, // 10MB
        })
    ],
    exitOnError: false
});

winston.addColors(customColors);

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding) {
        logger.info(message);
    }
};
